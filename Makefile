PROG = connect-proxy
OBJS = connect.o
PREFIX ?= /usr/local
QUIRKS = -DSOCKLEN_T=unsigned

all: build

build: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) $(QUIRKS) -c $<

install:
	install -Dm0755 $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG)
	ln -s $(PROG) $(DESTDIR)$(PREFIX)/bin/connect
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1
	cat $(PROG).1 | gzip >$(DESTDIR)$(PREFIX)/share/man/man1/$(PROG).1.gz
	install -Dt $(DESTDIR)$(PREFIX)/usr/share/licenses/$(PROG) -m644 LICENSE
	install -Dt $(DESTDIR)$(PREFIX)/usr/share/doc/$(PROG) -m644 manual.txt

clean:
	rm -f $(PROG) $(OBJS)

.PHONY: all install clean
